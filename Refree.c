#include <stdio.h>
#include <stdlib.h>
#include <signal.h> // for signal defines
#include <sys/types.h> // for stream type defines
#include <sys/socket.h>
#include <netinet/in.h> // for listen()
#include <netdb.h>
#include <string.h> // for string ops
#include <unistd.h> // for process management
#include <time.h> // for system time
#include "CommunicationHelper.h"

int createSocket();
void setSocketType(struct sockaddr_in* socket);
void bindNameToSocket(struct sockaddr_in* socket, int serverLen);
void startListeningToConnections(int clientFileDesc, struct sockaddr_in* clientSocketPtr, int clientAddrLen);
void printWelcomeMessage();
void parseClientData(char* data, int* choice);
char* getStringEquivalentOfData(int option);
void computeResult(int clientResponse, int cpuResponse);
void sendMessagetoClient(int clientSock, char* message);
void printResult(char* result);
void clearBuffer();
void parseConsoleInput(int option);
int getUserInput();
void setNgramType();
void initNGramDatabase();
void updateMovesHistory(int move);
void updateNgramArray(int move);
void shiftNgramArray();
void shiftMovesHistoryArray();
int generateMoveFromMovesHistory();
void printArray(int* arr, int size);
int getWinningChoices(int choice);
void printDebugStats();
void updateNgramTable(int* ngram, int prediction);
int checkForEntryInNgramTable(int* ngram, int prediction);
void printPredictionStats();
int generateRandomMove();
void trainNGram();
int predictMoveBasedOnTrainedDB(int* currentNgramPattern);
void buildNGramDatabase(int clientChoice);
void addToNgramTable(int* ngram, int prediction);

// global var
int serverFileDescriptor = -100;
char sendBuffer[BUFFER_SIZE];
char recieveBuffer[BUFFER_SIZE];
int ngrarmType;
int* movesHistoryDB;
int* nGramPattern;
int movesHistoryEntries = 0;
int nGramEntries = 0;

NGramStats* ngramStats;
int statEntries = 0;

int main()
{
	int serverLength, clientLength;
	int clientFileDescriptor = INVALID;
	srand ( (unsigned) time(NULL) );
	
	printWelcomeMessage();
	setNgramType();
	initNGramDatabase();
	trainNGram();
	
	struct sockaddr_in serverAddress; // server address
	struct sockaddr_in* serverSockAddrPtr = NULL; // pointer to address
	struct sockaddr_in* clientSockAddrPtr = NULL; // pointer to address
	
	serverSockAddrPtr = (struct sockaddr_in*) &serverAddress;
	serverLength = sizeof (serverAddress);
	clientLength = sizeof (serverAddress);
	
	serverFileDescriptor = createSocket();
	
	if (serverFileDescriptor == INVALID)
	{
		perror ("Refree:main");
		exit(1);
	}
	else
	{
		setSocketType (serverSockAddrPtr);
		bindNameToSocket (serverSockAddrPtr, serverLength);
		startListeningToConnections (clientFileDescriptor, clientSockAddrPtr, clientLength);
	}
	
	close (serverFileDescriptor);
	printf("closing server \n");
	return 1;
}

int createSocket()
{
	/** 
	 * int socket(int domain, int type, int protocol);
	 * @domain: Specifies the communications domain in which a socket is to be created.
	 * @type: Specifies the type of socket to be created.
	 * @protocol: Specifies a particular protocol to be used with the socket. Specifying a protocol of 0 causes socket() to use an unspecified default protocol appropriate for the requested socket type.
	 */
	return socket (AF_INET, SOCK_STREAM, 0);
}

void setSocketType(struct sockaddr_in* socket)
{
	bzero ( (char*) socket, sizeof (socket) ); // clears struct
	socket->sin_family = AF_INET;
	socket->sin_port = htons(MY_PORT);
	socket->sin_addr.s_addr = htonl(INADDR_ANY);
}

void bindNameToSocket(struct sockaddr_in* socket, int serverAddrLen)
{
	/**
	 * int bind(int socket, const struct sockaddr_in *address, socklen_t address_len);
	 * @socket: Specifies the file descriptor of the socket to be bound.
	 * @address: Points to a sockaddr structure containing the address to be bound to the socket. The length and format of the address depend on the address family of the socket.
	 * @address_len: Specifies the length of the sockaddr structure pointed to by the address argument.
	 * Upon successful completion, bind() shall return 0; otherwise, -1 shall be returned and errno set to indicate the error.
	 */
	bind (serverFileDescriptor, (struct sockaddr*) socket, serverAddrLen);
}

void startListeningToConnections(int clientFileDesc, struct sockaddr_in* clientSocketPtr, int clientAddrLen)
{
	/**
	 * int listen(int socket, int backlog)
	 * @socket: Specifies the file descriptor of the socket to be bound.
	 * @backlog: max que length for connections
	 * Upon successful completions, listen() shall return 0; otherwise, -1 shall be returned and errno set to indicate the error.
	 * */
	listen (serverFileDescriptor, MAX_CONNECTION_QUE);
	
	printf ("Waiting for player process to start....\n");
	
	int clientChoice = INVALID;
	int cpuChoice = INVALID;
	int anticipatedClientChoice = INVALID;
	
	int bytesRead = 0;
	
	do
	{
		clientFileDesc = accept (serverFileDescriptor, (struct sockaddr*) clientSocketPtr, (socklen_t*) &clientAddrLen);
		
		if (clientFileDesc == INVALID)
		{
			perror ("Refree:accept");
		}
		else
		{
			printf ("Client: ready.\n");
			printf ("Go\n");
			// client is connected to server, notify it to proceed
			clearBuffer();
			sendMessagetoClient (clientFileDesc, MESSAGE_GO);
			
			do
			{
				bzero ( recieveBuffer, sizeof (char) * BUFFER_SIZE );
				bytesRead = read ( clientFileDesc, recieveBuffer, BUFFER_SIZE );
				
				if (bytesRead < 0)
				{
					printf ("Refree: error reading from socket\n");
					perror ("Refree:read");
				}
				else if (bytesRead == 0)
				{
					printf ("Refree: no useful data to display\n");
				}
				else
				{
					clearBuffer();
					parseClientData (recieveBuffer, &clientChoice);
					if (clientChoice == STATS)
					{
						printPredictionStats();
						printDebugStats();
					}
					else
					{
						if (checkMoveValidity(clientChoice))
						{
							if (ngrarmType == RANDOM_NO_NGRAM)
							{
								anticipatedClientChoice = generateRandomMove();
							}
							else
							{
								anticipatedClientChoice = predictMoveBasedOnTrainedDB(nGramPattern);
								if (anticipatedClientChoice == INVALID)
								{
									// move is not predicted
									// look in moves history to find one
									anticipatedClientChoice = generateMoveFromMovesHistory();
									
									if (anticipatedClientChoice == INVALID)
									{
										// no matching pattern is found in moves history
										// generate random one
										anticipatedClientChoice = generateRandomMove();
									}
									// we couldn't predict, therefore update db with actual move
									updateNgramTable(nGramPattern, clientChoice);
									updateMovesHistory(clientChoice);
									updateNgramArray(clientChoice);
								}
							}
							// figure out the winning move from anticipated move
							cpuChoice = getWinningChoices(anticipatedClientChoice);
							bzero (sendBuffer, sizeof (char) * BUFFER_SIZE);
							strcat ( sendBuffer, getStringEquivalentOfData (cpuChoice) );
							printf ("\t Computer's move based on prediction: %s\n", sendBuffer);
							computeResult(clientChoice, cpuChoice);
						}
					}
					sendMessagetoClient(clientFileDesc, sendBuffer);
				}
			} while (bytesRead != 0);
		}
		
		close (clientFileDesc);
		
	} while (1);
}

void printWelcomeMessage()
{
	printf ("RockPaperScissors server is starting..\n");
	printf ("You can choose ctrl + c to end this process any time:\n");
	printf ("Choose an n-gram AI\n");
	printf ("\t MONOGRAM: %d\n", MONOGRAM);
	printf ("\t BIGRAM: %d\n", BIGRAM);
	printf ("\t TRIGRAM: %d\n", TRIGRAM);
	printf ("\t QUADGRAM: %d\n", QUADGRAM);
	printf ("\t DECIGRAM: %d\n", DECIGRAM);
	printf ("\t NO_NGRAM: %d\n", RANDOM_NO_NGRAM);
	printf ("Please enter your choice: ");
}

void parseClientData(char* data, int* choice)
{
	*choice = atoi (data);
	char* clientOption = getStringEquivalentOfData (*choice);
	printf ("---------------------------------------------\n");
	printf ("\t Client: %s\n", clientOption);
}

char* getStringEquivalentOfData(int option)
{
	char* choice = (char*) malloc (sizeof (char) * BUFFER_SIZE);
	
	if (option == PAPER)
	{
		choice = "PAPER";
	}
	else if (option == SCISSORS)
	{
		choice = "SCISSORS";
	}
	else if (option == ROCK)
	{
		choice = "ROCK";
	}
	else if (option == STATS)
	{
		choice = "STATS";
	}
	else
	{
		choice = "Unknown";
	}
	
	return choice;
}

// data sent in "Choice_Result" format
void computeResult(int clientResponse, int cpuResponse)
{
	char* winner = (char*) malloc (sizeof (char) * BUFFER_SIZE);
	
	if ( clientResponse < PAPER || clientResponse > ROCK || 
			cpuResponse < PAPER || cpuResponse > ROCK )
	{
		printf ("\t choice entered by player is invalid\n\n");
		return;
	}
	else if (clientResponse == cpuResponse)
	{
		strcat(sendBuffer, DELIMITER);
		strcat(sendBuffer, DRAW);
		printf ("\t Matching choices draw.\n\n");
		return;
	}
	else
	{
		if (clientResponse == PAPER && cpuResponse == ROCK)
		{
			printResult (USER_CLIENT);
			strcat(sendBuffer, DELIMITER);
			strcat(sendBuffer, WINNER_CLIENT);
			printf ("\t Paper beats rock (by covering it).\n\n");
			return;
		}
		else if (clientResponse == PAPER && cpuResponse == SCISSORS)
		{
			printResult (USER_CPU);
			strcat(sendBuffer, DELIMITER);
			strcat(sendBuffer, WINNER_CPU);
			printf ("\t Scissors beats paper (by cutting it).\n\n");
			return;
		}
		else if (clientResponse == ROCK && cpuResponse == PAPER)
		{
			printResult (USER_CPU);
			strcat(sendBuffer, DELIMITER);
			strcat(sendBuffer, WINNER_CPU);
			printf ("\t Paper beats rock (by covering it).\n\n");
			return;
		}
		else if (clientResponse == ROCK && cpuResponse == SCISSORS)
		{
			printResult (USER_CLIENT);
			strcat(sendBuffer, DELIMITER);
			strcat(sendBuffer, WINNER_CLIENT);
			printf ("\t Rock beats scissors (by blunting it).\n\n");
			return;
		}
		else if (clientResponse == SCISSORS && cpuResponse == PAPER)
		{
			printResult (USER_CLIENT);
			strcat(sendBuffer, DELIMITER);
			strcat(sendBuffer, WINNER_CLIENT);
			printf ("\t Scissors beats paper (by cutting it).\n\n");
			return;
		}
		else if (clientResponse == SCISSORS && cpuResponse == ROCK)
		{
			printResult (USER_CPU);
			strcat(sendBuffer, DELIMITER);
			strcat(sendBuffer, WINNER_CPU);
			printf ("\t Rock beats scissors (by blunting it).\n\n");
			return;
		}
	}
}

void sendMessagetoClient(int clientSock, char* message)
{
	strcpy (sendBuffer, message);
	send (clientSock, sendBuffer, BUFFER_SIZE, 0);
}

void printResult(char* result)
{
	printf ("\t %s wins\n", result);
}

void clearBuffer()
{
	bzero (sendBuffer, sizeof (char) * BUFFER_SIZE);
}

void setNgramType()
{
	parseConsoleInput(getUserInput());
}

int getUserInput()
{
	int option = INVALID;
	scanf ("%d", &option);
	return option;
}

void parseConsoleInput(int option)
{
	char* type = "";
	switch (option)
	{
		case MONOGRAM:
			type = "MONOGRAM";
			ngrarmType = option;
			break;
		case BIGRAM:
			type = "BIGRAM";
			ngrarmType = option;
			break;
		case TRIGRAM:
			type = "TRIGRAM";
			ngrarmType = option;
			break;
		case QUADGRAM:
			type = "QUADGRAM";
			ngrarmType = option;
			break;
		case DECIGRAM:
			type = "DECIGRAM";
			ngrarmType = option;
			break;
		case RANDOM_NO_NGRAM:
		default:
			type = "RANDOM_NO_NGRAM";
			ngrarmType = RANDOM_NO_NGRAM;
			break;
	}
	printf ("N-gram is set to: %s\n", type);
	if (ngrarmType != RANDOM_NO_NGRAM)
	{
		printf ("Last %d moves will be recorded, and out of those a pattern of %d will be matched\n", MAX_MOVES_FOR_NGRAM, ngrarmType);
	}
	else
	{
		printf("the CPU choices will be purely random\n");
	}
}

void initNGramDatabase()
{
	movesHistoryDB = (int*) malloc(sizeof(int) * MAX_MOVES_FOR_NGRAM);
	movesHistoryEntries = 0;
	
	nGramPattern = (int*) malloc(sizeof(int) * ngrarmType);
	nGramEntries = 0;
	
	statEntries = 0;
	ngramStats = NULL;
}

void updateNgramTable(int* ngram, int prediction)
{
	if (nGramEntries < ngrarmType)
	{
		// we need the ngram array to have at least n-entries
		return;
	}
	int ngramTableIndex = checkForEntryInNgramTable(ngram, prediction);
	if (ngramTableIndex != INVALID)
	{
		// we are good, update the count
		printf("found the entry in the table\n");
		ngramStats[ngramTableIndex].predictionCount++;
	}
	else
	{
		addToNgramTable(ngram, prediction);
		//printDebugStats();
		//printPredictionStats();
	}
}

int checkForEntryInNgramTable(int* ngram, int clientMove)
{
	if (ngramStats == NULL)
	{
		return INVALID;
	}
	
	for (int ii = 0; ii < statEntries; ii++)
	{
		if (checkIfArrayEqual(ngramStats[ii].pattern, ngram, ngrarmType))
		{
			if (ngramStats[ii].predictedMove == clientMove)
			{
				return ii;
			}
		}
	}
	return INVALID;
}

void printPredictionStats()
{
	printf("_____________________________________________________\n");
	printf("%-20s %-20s %s\n", "pattern", "prediction", "count");
	printf("_____________________________________________________\n");
	for (int ii = 0; ii < statEntries; ii++)
	{
		printArray(ngramStats[ii].pattern, ngramStats[ii].patternSize);
		printf("%20s", "");
		printf("%-20d %-20d\n", ngramStats[ii].predictedMove, ngramStats[ii].predictionCount);
	}
	printf("______________________________________________________\n");
}

void updateMovesHistory(int move)
{
	if (movesHistoryEntries < MAX_MOVES_FOR_NGRAM)
	{
		movesHistoryDB[movesHistoryEntries] = move;
		movesHistoryEntries++;
	}
	else
	{
		shiftMovesHistoryArray();
		movesHistoryDB[MAX_MOVES_FOR_NGRAM - 1] = move;
	}
}

void updateNgramArray(int move)
{
	if (nGramEntries < ngrarmType)
	{
		nGramPattern[nGramEntries] = move;
		nGramEntries++;
	}
	else
	{
		shiftNgramArray();
		nGramPattern[ngrarmType - 1] = move;
	}
}

void shiftNgramArray()
{
	//printf ("shiftNgramArray\n");
	int* newArray = (int*) malloc(sizeof(int) * ngrarmType);
	for (int ii = 1; ii < ngrarmType; ii++)
	{
		newArray[ii - 1] = nGramPattern[ii];
	}
	free(nGramPattern);
	nGramPattern = newArray;
}

void shiftMovesHistoryArray()
{
	int* newArray = (int*) malloc(sizeof(int) * MAX_MOVES_FOR_NGRAM);
	for (int ii = 1; ii < MAX_MOVES_FOR_NGRAM; ii++)
	{
		newArray[ii - 1] = movesHistoryDB[ii];
	}
	free(movesHistoryDB);
	movesHistoryDB = newArray;
}

int generateMoveFromMovesHistory()
{
	int matchIndex = 0;
	bool matchFound = false;
	int anticipatedChoice = INVALID;
	if (movesHistoryEntries <= ngrarmType || nGramEntries == 0)
	{
		return INVALID;
	}
	else
	{
		for (int ii = 0; ii < movesHistoryEntries; ii++)
		{
			for (int jj = 0; jj < nGramEntries; jj++)
			{
				if ((ii+jj < movesHistoryEntries)
					&& (movesHistoryDB[ii+jj] == nGramPattern[jj]))
				{
					if (jj == nGramEntries - 1)
					{
						matchFound = true;
						matchIndex = ii + jj + 1;
						ii = movesHistoryEntries;
					}
					continue;
				}
				else
				{
					break;
				}
			}
		}
	}
	
	if (matchFound == true && matchIndex < movesHistoryEntries - 1)
	{
		anticipatedChoice = movesHistoryDB[matchIndex];
		
		bzero (sendBuffer, sizeof (char) * BUFFER_SIZE);
		strcat ( sendBuffer, getStringEquivalentOfData (anticipatedChoice) );
		//printf ("\t Predicted player's move: %s\n", sendBuffer);
	}
	
	return anticipatedChoice;
}

void printArray(int* arr, int size)
{
	for (int ii = 0; ii < size; ii++)
	{
		printf("%d, ", arr[ii]);
	}
}

int getWinningChoices(int choice)
{
	switch(choice)
	{
		case ROCK:
			return PAPER;
			break;
		case PAPER:
			return SCISSORS;
			break;
		case SCISSORS:
		default:
			return ROCK;
			break;
	}
}

void printDebugStats()
{
	printf("moves db \n");
	printArray(movesHistoryDB, movesHistoryEntries);
	printf("\n");
	printf("ngram db \n");
	printArray(nGramPattern, nGramEntries);
	printf("\n");
}

int generateRandomMove()
{
	int maxRand = 3;
	int randOffset = 1;
	
	return ( ( rand() % maxRand ) + randOffset );
}

void trainNGram()
{
	printf("Starting to train n-gram data....\n");
	int clientChoice = INVALID;
	int anticipatedClientMove = INVALID;
	
	for (int ii = 0; ii < TRAIN_TURNS; ii++)
	{
		clientChoice = generateRandomMove();
		buildNGramDatabase(clientChoice);
		/*
		printf("random ngram: ");
		printArray(nGramPattern, ngrarmType);
		printf("\n");
		printf("random client choice: %d\n", clientChoice);
		* */
		
	}
	printf("Finished taining n-gram data\n");
}


int predictMoveBasedOnTrainedDB(int* currentNgramPattern)
{
	int move = INVALID;
	int count = INVALID;
	if (ngramStats == NULL)
	{
		return move;
	}
	
	for (int ii = 0; ii < statEntries; ii++)
	{
		if (checkIfArrayEqual(ngramStats[ii].pattern, nGramPattern, ngrarmType))
		{
			// go through all the entries and find the most probable one
			if (count < ngramStats[ii].predictionCount)
			{
				count = ngramStats[ii].predictionCount;
				move = ngramStats[ii].predictedMove;
			}
		}
	}
	return move;
}

void buildNGramDatabase(int clientChoice)
{
	int index = checkForEntryInNgramTable(nGramPattern, clientChoice);
	if (index == INVALID)
	{
		// move is not predicted
		// look in moves history to find one
		int anticipatedClientChoice = generateMoveFromMovesHistory();
		
		if (anticipatedClientChoice != INVALID)
		{
			// a matching pattern is found in moves history, add to the table
			addToNgramTable(nGramPattern, anticipatedClientChoice);
		}
	}
	else
	{
		//if the entry is found then update the count
		ngramStats[index].predictionCount++;
	}
	// if we are here just update the arrays, can't update the table
	updateMovesHistory(clientChoice);
	updateNgramArray(clientChoice);
	
	//printDebugStats();
	//printPredictionStats();
}

void addToNgramTable(int* ngram, int prediction)
{
	//printf("adding new entry\n");
	// add new entry
	// increase size of stats array
	NGramStats* temp = (NGramStats*) malloc(sizeof(NGramStats) * statEntries + 1);
	// copy previous data
	for (int ii = 0; ii < statEntries; ii++)
	{
		temp[ii].pattern = (int*) malloc(sizeof(int) * ngrarmType);
		copyArr(ngramStats[ii].pattern, temp[ii].pattern, ngrarmType);
		temp[ii].patternSize = ngramStats[ii].patternSize;
		temp[ii].predictedMove = ngramStats[ii].predictedMove;
		temp[ii].predictionCount = ngramStats[ii].predictionCount;
	}
	
	// add new entry to end of the new array
	
	temp[statEntries].pattern = (int*) malloc(sizeof(int) * ngrarmType);
	copyArr(ngram, temp[statEntries].pattern, ngrarmType);
	temp[statEntries].patternSize = ngrarmType;
	temp[statEntries].predictedMove = prediction;
	temp[statEntries].predictionCount = 1;
	
	for (int ii = 0; ii < statEntries; ii++)
	{
		//free(ngramStats[ii].pattern);
	}
	
	statEntries++;
	
	free(ngramStats);
	ngramStats = temp;
}
