#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h> // for process management
#include "CommunicationHelper.h"

void printWelcomeMessage();
void setupSocket();
int getUserInput();
void parseUserInput(int option);
void sendData(char* dataToSend);
void closeSocketConnection();
bool isSocketValid();
void getServerMessage();
void parseServerMessage(char* data);
void parseString(char* myString, char* choice, char* result);

// global var for socket
int socketFileDescriptor = -100;
char buffer[BUFFER_SIZE];

int main()
{
	setupSocket();
	getServerMessage();
	printf ("Closing Client\n");
	return 1;
}

void printWelcomeMessage()
{
	printf ("---------------------------------------------\n");
	printf ("Welcome to the Paper, Scissors, Rock game.\n");
	printf ("You can choose:\n");
	printf ("\t 1-Paper \n");
	printf ("\t 2-Scissors \n");
	printf ("\t 3-Rock \n");
	printf ("\t 4-Print Ngram Stats \n");
	printf ("\t 5-Quit \n");
	printf ("Go\n");
	printf ("Please enter your choice: ");
}

int getUserInput()
{
	int option = -1;
	scanf ("%d", &option);
	return option;
}

void parseUserInput(int option)
{
	if (option == PAPER)
	{
		sendData(USERDATA_PAPER);
	}
	else if (option == SCISSORS)
	{
		sendData(USERDATA_SCISSORS);
	}
	else if (option == ROCK)
	{
		sendData(USERDATA_ROCK);
	}
	else if (option == STATS)
	{
		sendData(USERDATA_STATS);
	}
	else if (option == EXIT)
	{
		closeSocketConnection();
		exit (1);
	}
	else
	{
		printf ("You have entered wrong choice\n");
		sendData(USERDATA_BAD);
	}
}

void setupSocket()
{
	
	struct sockaddr_in server_addr;
	struct hostent *server;
	
	socketFileDescriptor = socket (AF_INET, SOCK_STREAM, 0);
	
	isSocketValid();

	server_addr.sin_family = AF_INET;
	server = gethostbyname(MY_SERVERNAME);
	
	if (server == 0)
	{
		printf ("unknown host: %s", MY_SERVERNAME);
		exit (1);
	}
	
	bcopy ( server->h_addr, &server_addr.sin_addr, server->h_length );
	
	server_addr.sin_port = htons (MY_PORT);
	
	int connection = connect ( socketFileDescriptor, (struct sockaddr*) &server_addr, sizeof (server_addr) );
	
	if (connection < 0)
	{
		perror ("Player::connect");
		exit (1);
	}
}

void closeSocketConnection()
{
	if ( isSocketValid() )
	{
		close (socketFileDescriptor);
	}
}

void sendData(char* dataToSend)
{
	if ( write (socketFileDescriptor, dataToSend, strlen (dataToSend) ) < 0 )
	{
		perror ("Player:write");
	}
}

void getServerMessage()
{
	bzero ( buffer, sizeof (char) * BUFFER_SIZE );
	int bytesRead = 0;
	
	do
	{
		bytesRead = recv (socketFileDescriptor, buffer, BUFFER_SIZE, 0);
		
		if (bytesRead < 0)
		{
			printf ("client: error reading from socket\n");
			perror ("Player:recv");
		}
		else if (bytesRead == 0)
		{
			printf ("client: no useful data to display\n");
		}
		else
		{
			//~ printf ("client data is: %s\n", buffer);
			parseServerMessage(buffer);
		}
		
	} while (bytesRead != 0);
}

bool isSocketValid()
{
	if (socketFileDescriptor < 0)
	{
		perror ("Player::socket");
		return false;
	}
	
	return true;
}

void parseServerMessage(char* data)
{
	// server data sent in "Choice_Result" format
	
	char* result = malloc (sizeof (char) * BUFFER_SIZE);
	char* cpu_choice = malloc (sizeof (char) * BUFFER_SIZE);
	
	if ( strchr (data, DELIMITER[0]) != NULL )
	{
		parseString(data, cpu_choice, result);
		printf ("The other player chose %s, you %s\n\n", cpu_choice, result);
	}
	
	printWelcomeMessage();
	parseUserInput ( getUserInput() );
}

void parseString(char* myString, char* choice, char* result)
{
	int count = 0;
	int splitPosition = 0;
	while (myString[count] != '\0')
	{
		if ( myString[count] == DELIMITER[0] )
		{
			splitPosition = count;
		}
		count++;
	}
	strncpy(choice, myString, splitPosition);
	
	// +1 to skip '_'
	strncpy(result, &myString[splitPosition + 1], (count - (splitPosition + 1)));
}
