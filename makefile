all: Refree Player

Refree: Refree.o
	gcc Refree.o -o Refree
Refree.o: Refree.c CommunicationHelper.h
	gcc -c Refree.c
Player: Player.o
	gcc Player.o -o Player
Player.o: Player.c CommunicationHelper.h
	gcc -c Player.c

.PHONY: clean
.PHONY: all

clean:
	-rm *.o
	-rm Refree
	-rm Player
