BitBucket URL:
https://bitbucket.org/happy_go_lucky/rockpaperscissors/src/master/

This a rock paper scissors game using C. It uses n-grarms move predictions.
Build the game using the makefile. Run it with make utility with command 

make -f makefile

It is going to create two executable files
1. Refree
2. Player

Steps:

1. Run the Refree process first by following command in one bash window ./Refree
2. choose one of the options from menu.
3, Run the Player process on the same machine on a separate bash window ./Player
4. It automatically joins Refree, and you should see a message.
5. switch the focus to Player process and start playing.
6. Results are displayed on both consoles.

Player Instructions:
Follow onscreen instructions.